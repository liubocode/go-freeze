# go-freeze

#### 介绍
方便项目的依赖打包到离线环境中使用，为了让包更加精简，只打包目标项目的go.mod

#### 使用

```
go install gitee.com/liubocode/go-freeze
go-freeze -z -o /yourpath go.mod
```